var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var pg = require('pg');
var session = require('express-session');
require('dotenv').config();
// var cookieSession = require('cookie-session');

var apiRouter = require('./routes/api');

var app = express();

const corsOptions = {
    origin: process.env.CORS_ORIGIN,
    credentials: true,
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// app.set('trust proxy', 1);

const pgSession = require('connect-pg-simple')(session);
const pgPool = new pg.Pool({
    // Insert pool options here
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
});

app.use(session({
    store: new pgSession({
        pool: pgPool,           // Connection pool
        tableName: 'Sessions'   // Use another table-name than the default "session" one
        // Insert connect-pg-simple options here
    }),
    secret: 'fhrgfgrfrty84fwir767',
    saveUninitialized: false,
    resave: false,
    cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 } // 30 days
    // Insert express-session options here
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors(corsOptions));
app.use('/', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
