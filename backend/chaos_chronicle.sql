--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5 (Ubuntu 13.5-0ubuntu0.21.10.1)
-- Dumped by pg_dump version 13.5 (Ubuntu 13.5-0ubuntu0.21.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Battles; Type: TABLE; Schema: public; Owner: chaos_chronicle
--

CREATE TABLE public."Battles" (
    id integer NOT NULL,
    pack_winner_id integer,
    pack_loser_id integer,
    happened_at timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Battles" OWNER TO chaos_chronicle;

--
-- Name: Battles_id_seq; Type: SEQUENCE; Schema: public; Owner: chaos_chronicle
--

CREATE SEQUENCE public."Battles_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Battles_id_seq" OWNER TO chaos_chronicle;

--
-- Name: Battles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chaos_chronicle
--

ALTER SEQUENCE public."Battles_id_seq" OWNED BY public."Battles".id;


--
-- Name: HeroPacks; Type: TABLE; Schema: public; Owner: chaos_chronicle
--

CREATE TABLE public."HeroPacks" (
    id integer NOT NULL,
    hero_id integer,
    pack_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."HeroPacks" OWNER TO chaos_chronicle;

--
-- Name: HeroPacks_id_seq; Type: SEQUENCE; Schema: public; Owner: chaos_chronicle
--

CREATE SEQUENCE public."HeroPacks_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."HeroPacks_id_seq" OWNER TO chaos_chronicle;

--
-- Name: HeroPacks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chaos_chronicle
--

ALTER SEQUENCE public."HeroPacks_id_seq" OWNED BY public."HeroPacks".id;


--
-- Name: Heroes; Type: TABLE; Schema: public; Owner: chaos_chronicle
--

CREATE TABLE public."Heroes" (
    id integer NOT NULL,
    name character varying(255),
    image_path character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Heroes" OWNER TO chaos_chronicle;

--
-- Name: Heroes_id_seq; Type: SEQUENCE; Schema: public; Owner: chaos_chronicle
--

CREATE SEQUENCE public."Heroes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Heroes_id_seq" OWNER TO chaos_chronicle;

--
-- Name: Heroes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chaos_chronicle
--

ALTER SEQUENCE public."Heroes_id_seq" OWNED BY public."Heroes".id;


--
-- Name: Packs; Type: TABLE; Schema: public; Owner: chaos_chronicle
--

CREATE TABLE public."Packs" (
    id integer NOT NULL,
    hash character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Packs" OWNER TO chaos_chronicle;

--
-- Name: Packs_id_seq; Type: SEQUENCE; Schema: public; Owner: chaos_chronicle
--

CREATE SEQUENCE public."Packs_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Packs_id_seq" OWNER TO chaos_chronicle;

--
-- Name: Packs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chaos_chronicle
--

ALTER SEQUENCE public."Packs_id_seq" OWNED BY public."Packs".id;


--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: chaos_chronicle
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO chaos_chronicle;

--
-- Name: Battles id; Type: DEFAULT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."Battles" ALTER COLUMN id SET DEFAULT nextval('public."Battles_id_seq"'::regclass);


--
-- Name: HeroPacks id; Type: DEFAULT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."HeroPacks" ALTER COLUMN id SET DEFAULT nextval('public."HeroPacks_id_seq"'::regclass);


--
-- Name: Heroes id; Type: DEFAULT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."Heroes" ALTER COLUMN id SET DEFAULT nextval('public."Heroes_id_seq"'::regclass);


--
-- Name: Packs id; Type: DEFAULT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."Packs" ALTER COLUMN id SET DEFAULT nextval('public."Packs_id_seq"'::regclass);


--
-- Data for Name: Battles; Type: TABLE DATA; Schema: public; Owner: chaos_chronicle
--

COPY public."Battles" (id, pack_winner_id, pack_loser_id, happened_at, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: HeroPacks; Type: TABLE DATA; Schema: public; Owner: chaos_chronicle
--

COPY public."HeroPacks" (id, hero_id, pack_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Heroes; Type: TABLE DATA; Schema: public; Owner: chaos_chronicle
--

COPY public."Heroes" (id, name, image_path, "createdAt", "updatedAt") FROM stdin;
1	Аврора	images/avatars/3bc00e241b88af528be0d986e4a62c2c.png	2022-02-14 18:40:18.556+00	2022-02-14 18:40:18.556+00
3	Айзек	images/avatars/70c11c7015be636aae651fa03540a6c0.png	2022-02-14 18:44:21.95+00	2022-02-14 18:44:21.95+00
5	Альванор	images/avatars/d91310c6a5ebe0813351e6839cf70d9a.png	2022-02-14 19:07:07.706+00	2022-02-14 19:07:07.706+00
6	Альмира	images/avatars/ce90fa1493a5a2fe19377a393b6df766.png	2022-02-14 19:07:48.551+00	2022-02-14 19:07:48.551+00
8	Арахна	images/avatars/de00a845b317f490a976f75715ca5ffb.png	2022-02-14 19:09:25.447+00	2022-02-14 19:09:25.447+00
9	Артемис	images/avatars/e85357623cbdedbe6df223cfa9c939fe.png	2022-02-14 19:11:17.249+00	2022-02-14 19:11:17.249+00
10	Астарот	images/avatars/5f13a05c2ac527c92e03643fa702063e.png	2022-02-14 19:11:39.613+00	2022-02-14 19:11:39.613+00
11	Астрид и Лукас	images/avatars/1b0a295bb2466ded68f5eb9ddf080329.png	2022-02-14 19:12:31.938+00	2022-02-14 19:12:31.938+00
12	Безликий	images/avatars/b629f0ae56ecea6aa1882d9e0d9c7dc0.png	2022-02-14 19:12:55.35+00	2022-02-14 19:12:55.35+00
13	Галахад	images/avatars/73bd4ec0164b7900c71fba26295612da.png	2022-02-14 19:13:32.957+00	2022-02-14 19:13:32.957+00
14	Гелиос	images/avatars/7228f99432d4bd130278ec9628f896d4.png	2022-02-14 19:14:45.503+00	2022-02-14 19:14:45.503+00
15	Данте	images/avatars/9bb0184e16d3de69f7edcab5c3a6e8b8.png	2022-02-14 19:15:09.047+00	2022-02-14 19:15:09.047+00
16	Джет	images/avatars/f0e2203d37e42eb0f941aa615f47ee1d.png	2022-02-14 19:15:44.438+00	2022-02-14 19:15:44.438+00
17	Джинджер	images/avatars/4bfa72391cc6577106213b4652aae30a.png	2022-02-14 19:16:20.905+00	2022-02-14 19:16:20.905+00
18	Джу	images/avatars/061d2a454851597afc7ef0f16d909847.png	2022-02-14 19:16:51.37+00	2022-02-14 19:16:51.37+00
19	Дориан	images/avatars/9bbf5483e49f24bcc4dcca449cefb16e.png	2022-02-14 19:17:32.601+00	2022-02-14 19:17:32.601+00
20	Зири	images/avatars/19d71a6c18827a05257dadda88db4f4c.png	2022-02-14 19:18:18.663+00	2022-02-14 19:18:18.663+00
21	Исмаил	images/avatars/48bd31559eca744b97645c4488a11101.png	2022-02-14 19:19:42.81+00	2022-02-14 19:19:42.81+00
22	Йорген	images/avatars/8dae76508cc98d4bc2e018ddb3781cbf.png	2022-02-14 19:20:18.702+00	2022-02-14 19:20:18.702+00
23	Кай	images/avatars/10b980059575cc5dc13d92e7f49efa0f.png	2022-02-14 19:21:20.836+00	2022-02-14 19:21:20.836+00
24	Карх	images/avatars/6de8885327eb7eaa5abac4cc846729b1.png	2022-02-14 19:21:53.158+00	2022-02-14 19:21:53.158+00
25	Кира	images/avatars/95a1af8a1c54f42a2d7f7a0bb434138f.png	2022-02-14 19:22:16.261+00	2022-02-14 19:22:16.261+00
26	Корвус	images/avatars/27585f06e5c6b64372877d2f9aafdc43.png	2022-02-14 19:23:44.579+00	2022-02-14 19:23:44.579+00
27	Корнелиус	images/avatars/eacc91c30142c38fd6de1c75299a33cd.png	2022-02-14 19:30:35.321+00	2022-02-14 19:30:35.321+00
28	Криста	images/avatars/0eab61d781e1d8614773f5ac51c0bf76.png	2022-02-14 19:31:47.028+00	2022-02-14 19:31:47.028+00
29	Ксеша	images/avatars/250bf252f6691a93b9b3b32a79b281f3.png	2022-02-14 19:32:28.453+00	2022-02-14 19:32:28.453+00
30	Ларс	images/avatars/c113f0a4be3a254c549df5c221e12224.png	2022-02-14 19:35:04.735+00	2022-02-14 19:35:04.735+00
31	Лилит	images/avatars/f9064d12f47e7930b77521f0bbfadd51.png	2022-02-14 19:35:37.262+00	2022-02-14 19:35:37.262+00
32	Лиэн	images/avatars/227e6799e855dfe8f94d6e2f5de6e989.png	2022-02-14 19:36:23.209+00	2022-02-14 19:36:23.209+00
33	Лютер	images/avatars/ef5d928624f3c1863f868fd1e1017234.png	2022-02-14 19:37:20.403+00	2022-02-14 19:37:20.403+00
34	Майя	images/avatars/6a5df61e180233cad4d6ecbf6f1cd7d1.png	2022-02-14 19:37:51.136+00	2022-02-14 19:37:51.136+00
35	Маркус	images/avatars/9b7089ab5cab8871a74d6a46f31dac08.png	2022-02-14 19:38:18.813+00	2022-02-14 19:38:18.813+00
36	Марта	images/avatars/7845d55486eb710a40200ef39071dc35.png	2022-02-14 19:38:59.398+00	2022-02-14 19:38:59.398+00
37	Моджо	images/avatars/f8b335c99d6e99c644439bef81e4f36d.png	2022-02-14 19:39:40.734+00	2022-02-14 19:39:40.734+00
38	Морриган	images/avatars/ff0a9caf921e0330aff6710a02e368c6.png	2022-02-14 19:40:26.308+00	2022-02-14 19:40:26.308+00
39	Небула	images/avatars/de19bf71a3fffb16117f72edfab90e82.png	2022-02-14 19:41:13.949+00	2022-02-14 19:41:13.949+00
40	Орион	images/avatars/b90ea5d50fe495dd2695ebe1966b47da.png	2022-02-14 19:41:46.725+00	2022-02-14 19:41:46.725+00
41	Пеппи	images/avatars/fbf15b6aa444798a55a522196e0d6f38.png	2022-02-14 19:42:08.542+00	2022-02-14 19:42:08.542+00
42	Руфус	images/avatars/1bfcd089d8eb3e7fc3d9a1d262c7fed4.png	2022-02-14 19:42:54.626+00	2022-02-14 19:42:54.626+00
43	Сатори	images/avatars/1e4dfbdd88c40667cf402027c2f673a3.png	2022-02-14 19:43:29.657+00	2022-02-14 19:43:29.657+00
44	Себастьян	images/avatars/8d614528be8a90c02d7285d41ba3bc86.png	2022-02-14 19:44:01.931+00	2022-02-14 19:44:01.931+00
45	Селеста	images/avatars/4d149f4eafdc9b4f1f7d1213139e829b.png	2022-02-14 19:44:33.572+00	2022-02-14 19:44:33.572+00
46	Сорвиголова	images/avatars/100219978ccdf753477d8751cef44059.png	2022-02-14 19:44:51.155+00	2022-02-14 19:44:51.155+00
47	Судья	images/avatars/9c059882a1d0f200417ddfe62a0150bd.png	2022-02-14 19:45:21.299+00	2022-02-14 19:45:21.299+00
48	Темная Звезда	images/avatars/185e3f41edf9ebed28fbb2f005357c78.png	2022-02-14 19:45:52.72+00	2022-02-14 19:45:52.72+00
49	Тесак	images/avatars/78cf3c56b2f4b100288e1e62e3156948.png	2022-02-14 19:46:24.662+00	2022-02-14 19:46:24.662+00
50	Тея	images/avatars/e618c7de8b9c23cb4633647176be53b5.png	2022-02-14 19:46:37.212+00	2022-02-14 19:46:37.212+00
51	Тристан	images/avatars/62ce07240290fd8c2fd3d8bface96d77.png	2022-02-14 19:47:15.531+00	2022-02-14 19:47:15.531+00
52	Фобос	images/avatars/4d769b973acf9f6525643b07758e736a.png	2022-02-14 19:47:30.787+00	2022-02-14 19:47:30.787+00
53	Фокс	images/avatars/1fec8e9d935af123304ca5b76a8e0589.png	2022-02-14 19:47:54.609+00	2022-02-14 19:47:54.609+00
54	Хайди	images/avatars/7484000fd88780dc54719ea9f7eb9a03.png	2022-02-14 19:48:11.183+00	2022-02-14 19:48:11.183+00
55	Цин Мао	images/avatars/576deb36ca5b7b951e11867be1519365.png	2022-02-14 19:48:36.864+00	2022-02-14 19:48:36.864+00
56	Чабба	images/avatars/67747bfbc2e9771f6e335ce5c5ae94c2.png	2022-02-14 19:49:01.773+00	2022-02-14 19:49:01.773+00
57	Эльмир	images/avatars/3a20f16e61e063823078f8b8ac699411.png	2022-02-14 19:49:34.602+00	2022-02-14 19:49:34.602+00
58	Ясмин	images/avatars/c7e48daf7b625578e14955cbcd759522.png	2022-02-14 19:50:15.467+00	2022-02-14 19:50:15.467+00
7	Андвари	images/avatars/4cd016556318af5183889a6c5bbfedcf.png	2022-02-14 19:08:29.453+00	2022-02-14 20:06:50.519+00
\.


--
-- Data for Name: Packs; Type: TABLE DATA; Schema: public; Owner: chaos_chronicle
--

COPY public."Packs" (id, hash, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: chaos_chronicle
--

COPY public."SequelizeMeta" (name) FROM stdin;
20220204182103-create-heroes.js
20220204182116-create-battles.js
20220204193353-create-pack.js
20220204193609-create-hero-pack.js
\.


--
-- Name: Battles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: chaos_chronicle
--

SELECT pg_catalog.setval('public."Battles_id_seq"', 1, false);


--
-- Name: HeroPacks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: chaos_chronicle
--

SELECT pg_catalog.setval('public."HeroPacks_id_seq"', 1, false);


--
-- Name: Heroes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: chaos_chronicle
--

SELECT pg_catalog.setval('public."Heroes_id_seq"', 58, true);


--
-- Name: Packs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: chaos_chronicle
--

SELECT pg_catalog.setval('public."Packs_id_seq"', 1, false);


--
-- Name: Battles Battles_pkey; Type: CONSTRAINT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."Battles"
    ADD CONSTRAINT "Battles_pkey" PRIMARY KEY (id);


--
-- Name: HeroPacks HeroPacks_pkey; Type: CONSTRAINT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."HeroPacks"
    ADD CONSTRAINT "HeroPacks_pkey" PRIMARY KEY (id);


--
-- Name: Heroes Heroes_pkey; Type: CONSTRAINT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."Heroes"
    ADD CONSTRAINT "Heroes_pkey" PRIMARY KEY (id);


--
-- Name: Packs Packs_pkey; Type: CONSTRAINT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."Packs"
    ADD CONSTRAINT "Packs_pkey" PRIMARY KEY (id);


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: chaos_chronicle
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- PostgreSQL database dump complete
--

