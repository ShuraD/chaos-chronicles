var express = require('express');
var router = express.Router();
const sharp = require('sharp');
const path = require('path');
const getPixels = require('get-pixels');
const pixels = require('image-pixels');
const jimp = require('jimp');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const fs = require('fs');
const db = require('../src/orm/models/index');

/* GET test modules. */
router.get('/', async function (req, res, next) {
    const file = path.resolve(__dirname, 'msg790537572-22330.jpg');
    // const image = sharp(file);
    // const stats = await image.stats();
    // console.log(stats);
    // getPixels(file, (error, data) => {
    //   console.log(data.shape.slice());
    //   res.send(data);
    // });
    // const { data } = await pixels(file);
    // console.log(data[0]);
    jimp.read(file, (err, image) => {
        res.send({
            width: image.getWidth(),
            height: image.getHeight(),
            pixelData: {
                x: 1,
                y: 1,
                data: jimp.intToRGBA(image.getPixelColor(1, 1)),
            }
        });
    });
    // res.send();
});

router.get('/heroes', async function (req, res, next) {
    try {
        const HeroesModel = db['Heroes'];
        const heroes = await HeroesModel.findAll({ order: [[ 'id', 'ASC' ]], raw: true });
        return res.json({
            status: 'ok',
            data: heroes,
        });
    } catch (e) {
        // TODO: process error
        res.status(400).send(e.message);
    }
});

/* POST hero save */
router.post('/hero/save', upload.single('avatar'), async function (req, res, next) {
    const id = req.body.id;
    const file = req.file;
    const name = req.body.name;

    try {
        const ext = '.png';
        const oldPath = 'uploads/' + file.filename;
        const newPath  = 'public/images/avatars/' + file.filename + ext;
        const HeroesModel = db['Heroes'];
        let hero = null;
        if (id) {
            hero = await HeroesModel.findByPk(id);
            if (!hero) {
                throw new Error('Invalid hero id: ', id);
            }
            // TODO: remove old file
            fs.unlinkSync('public/' + hero.image_path);
            hero.name = name;
            hero.image_path = 'images/avatars/' + file.filename + ext;
        } else {
            // TODO: save hero info
            hero = HeroesModel.build({
                name,
                image_path: 'images/avatars/' + file.filename + ext,
            });
        }
        // TODO: move file
        fs.renameSync(oldPath, newPath);
        await hero.save();

        return res.json({
            hero,
        });
    } catch (e) {
        // TODO: process error
        return res.status(400).json({ stack: e.stack });
    }
});

router.post('/hero/delete', async function (req, res, next) {
    const id = req.body.id;
    try {
        const HeroesModel = db['Heroes'];
        const hero = await HeroesModel.findByPk(id);
        if (!hero) {
            throw new Error('Invalid hero id: ' + id);
        }
        await hero.destroy();

        return res.json({
            id,
        });
    } catch (e) {
        // TODO: process error
        return res.status(400).send(e.message);
    }
});

router.get('/battles', async function (req, res, next) {
    try {
        const BattleModel = db['Battles'];
        const battles = await BattleModel.findAll({ order: [[ 'id', 'ASC' ]], raw: true });

        return res.status(200).send({
            status: 'ok',
            data: battles,
        });
    } catch (e) {
        // TODO: process error
        return res.status(400).json({ stack: e.stack });
    }
});

router.post('/battle/save', async function (req, res, next) {
    try {
        const id = req.body.id;
        const packs = req.body.packs;
        // TODO: validate pack info
        const BattleModel = db['Battles'];
        const battle = await BattleModel.create({
            position_1: packs.positions_1["1"].id,
            position_2: packs.positions_1["2"].id,
            position_3: packs.positions_1["3"].id,
            position_4: packs.positions_1["4"].id,
            position_5: packs.positions_1["5"].id,
            position_6: packs.positions_2["1"].id,
            position_7: packs.positions_2["2"].id,
            position_8: packs.positions_2["3"].id,
            position_9: packs.positions_2["4"].id,
            position_10: packs.positions_2["5"].id,
        });
        return res.json(battle);
    } catch (e) {
        // TODO: process error
        return res.status(400).json({ stack: e.stack });
    }
});

router.delete('/battle/:id', async function (req, res, next) {
    try {
        const id = req.params.id;
        const BattleModel = db['Battles'];
        const battle = await BattleModel.findByPk(id);
        if (battle) {
            await battle.destroy();
            return res.send('ok');
        } else {
            return res.status(400).send('No battle exist');
        }
    } catch (e) {
        return res.status(400).json({stack: e.stack});
    }
});

router.post('/login', async function (req, res, next) {
    try {
        if (!req.session.user_id) {
            const username = req.body.username;
            const password = req.body.password;

            const User = db['User'];
            const user = await User.findOne({ where: { name: username } });
            if (!username || !user) {
                return res.send({
                    status: 'error',
                    reason: 'Invalid user name or password',
                });
            }
            if (!password || !user.isPasswordValid(password)) {
                return res.send({
                    status: 'error',
                    reason: 'Invalid user name or password',
                });
            }
            req.session.user_id = user.id;
        }

        return res.send({
            status: 'ok',
            session_id: req.sessionID,
        });
    } catch (e) {
        // TODO: process error
        return res.send({
            status: 'error',
            reason: e.message,
            stack: e.stack,
        });
    }
});

router.post('/login/auto', async function (req, res, next) {
    try {
        return res.send({
            status: 'ok',
            session_id: req.session.user_id ? req.sessionId : '',
        });
    } catch (e) {
        // TODO: process error
        return res.send({
            status: 'error',
            reason: e.message,
            stack: e.stack,
        });
    }
});

module.exports = router;
