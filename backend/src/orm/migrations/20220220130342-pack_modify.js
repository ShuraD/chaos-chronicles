'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        const type = Sequelize.INTEGER;
        const allowNull = false;

        return Promise.all([
            queryInterface.removeColumn('Battles', 'pack_winner_id'),
            queryInterface.removeColumn('Battles', 'pack_loser_id'),
            queryInterface.removeColumn('Battles', 'happened_at'),
            queryInterface.addColumn('Battles', 'position_1', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_2', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_3', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_4', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_5', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_6', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_7', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_8', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_9', { type, allowNull }),
            queryInterface.addColumn('Battles', 'position_10', { type, allowNull }),
        ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
        return Promise.all([
            queryInterface.removeColumn('Battles', 'position_1'),
            queryInterface.removeColumn('Battles', 'position_2'),
            queryInterface.removeColumn('Battles', 'position_3'),
            queryInterface.removeColumn('Battles', 'position_4'),
            queryInterface.removeColumn('Battles', 'position_5'),
            queryInterface.removeColumn('Battles', 'position_6'),
            queryInterface.removeColumn('Battles', 'position_7'),
            queryInterface.removeColumn('Battles', 'position_8'),
            queryInterface.removeColumn('Battles', 'position_9'),
            queryInterface.removeColumn('Battles', 'position_10'),
            queryInterface.addColumn('Battles', 'pack_winner_id', { type: Sequelize.INTEGER }),
            queryInterface.addColumn('Battles', 'pack_loser_id', { type: Sequelize.INTEGER }),
            queryInterface.addColumn('Battles', 'happened_at', { type: Sequelize.DATE }),
        ]);
    }
};
