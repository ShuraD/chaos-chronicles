'use strict';
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Sessions', {
            sid: {
                allowNull: false,
                primaryKey: true,
                type: Sequelize.STRING,
            },
            sess: {
                allowNull: false,
                type: Sequelize.JSON,
            },
            expire: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        }, {
            indexes: [
                {
                    name: 'IDX_session_expire',
                    fields: ['expire'],
                }
            ]
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Sessions');
    }
};