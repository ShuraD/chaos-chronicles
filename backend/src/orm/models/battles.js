'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Battles extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }

    Battles.init({
        position_1: DataTypes.INTEGER,
        position_2: DataTypes.INTEGER,
        position_3: DataTypes.INTEGER,
        position_4: DataTypes.INTEGER,
        position_5: DataTypes.INTEGER,
        position_6: DataTypes.INTEGER,
        position_7: DataTypes.INTEGER,
        position_8: DataTypes.INTEGER,
        position_9: DataTypes.INTEGER,
        position_10: DataTypes.INTEGER,
    }, {
        sequelize,
        modelName: 'Battles',
    });
    return Battles;
};