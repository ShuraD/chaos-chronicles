'use strict';
const {
    Model
} = require('sequelize');
const crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
        isPasswordValid(password) {
            return crypto.pbkdf2Sync(password, '', 1000, 64, 'sha512').toString('hex') === this.hash;
        }
    }

    User.init({
        name: DataTypes.STRING,
        hash: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'User',
    });
    return User;
};