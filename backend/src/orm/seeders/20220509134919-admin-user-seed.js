'use strict';

const crypto = require('crypto');

module.exports = {
    async up(queryInterface, Sequelize) {
        const now = new Date();
        await queryInterface.bulkInsert('Users', [
            {
                name: 'admin',
                hash: crypto.pbkdf2Sync('Bh:*.67D!u', '', 1000, 64, 'sha512').toString('hex'),
                createdAt: now,
                updatedAt: now,
            }
        ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
