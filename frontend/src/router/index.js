import {createRouter, createWebHashHistory} from 'vue-router'
import Heroes from '../views/Heroes.vue'
import Packs from '../views/Packs.vue'
import Battles from '../views/Battles.vue'
import Login from '../views/Login.vue'

const routes = [
    {
        path: '/',
        name: 'Base',
        redirect: {
            name: 'Heroes',
        }
    },
    {
        path: '/heroes',
        name: 'Heroes',
        component: Heroes,
    },
    {
        path: '/packs',
        name: 'Packs',
        component: Packs,
    },
    {
        path: '/battles',
        name: 'Battles',
        component: Battles,
    },
    // {
    //     path: '/login',
    //     name: 'Login',
    //     component: Login,
    // },
    {
        path: '/admin',
        name: 'Admin',
        component: Login,
    }
    // {
    //   path: '/about',
    //   name: 'About',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: function () {
    //     return import(/* webpackChunkName: "about" */ '../views/About.vue')
    //   }
    // }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router
