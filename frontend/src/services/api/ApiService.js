const axios = require('axios');

const API_URL = '/api';

module.exports.loadHeroes = function () {
    return axios.get(`${API_URL}/heroes`);
};
module.exports.deleteHero = function (heroId) {
    return axios.post(`${API_URL}/hero/delete`, { id: heroId });
};
module.exports.saveHero = function (formData) {
    return axios.post(
        `${API_URL}/api/hero/save`,
        formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
    );
};
module.exports.loadBattles = function () {
    return axios.get(`${API_URL}/battles`);
};
module.exports.saveBattle = function (data) {
    return axios.post(`${API_URL}/battle/save`, data);
};
module.exports.login = function (data) {
    return axios.post(`${API_URL}/login`, data, { withCredentials: true }) ;
};
module.exports.removeBattle = function (id) {
    return axios.delete(`${API_URL}/battle/${id}`);
};
module.exports.autoLogin = function (data) {
    return axios.post(`${API_URL}/login/auto`, data, { withCredentials: true }) ;
};
