import { createStore } from 'vuex'
import ApiService from '../services/api/ApiService';

export default createStore({
    state: {
        isProduction: true,
        isInitialized: false,
        heroes: [],
        selectedHero: null,
        heroData: null,
        battles: [],
        battleData: null,
        isAdmin: false,
        username: null,
        password: null,
        token: null,
        loginError: null,
        sessionId: null,
    },
    mutations: {
        setHeroes:       ( state, heroes )   => state.heroes = heroes,
        setSelectedHero: ( state, hero )     => state.selectedHero = hero,
        setHeroData:     ( state, formData ) => state.heroData = formData,
        setBattles:      ( state, battles )  => state.battles = battles,
        setBattleData:   ( state, data )     => state.battleData = data,
        setIsAdmin:      ( state, value )    => state.isAdmin = value,
        setLoginError:   ( state, error )    => state.loginError = error,
        setSessionId:    ( state, id )       => state.sessionId = id,
    },
    getters: {
        isProduction: state => state.isProduction,
        heroes:       state => state.heroes,
        selectedHero: state => state.selectedHero,
        battles:      state => state.battles,
        isAdmin:      state => state.isAdmin,
        loginError:   state => state.loginError,
        sessionId:    state => state.sessionId,
    },
    actions: {
        init({ state, dispatch }) {
            if (!state.isInitialized) {
                state.isInitialized = true;
                dispatch('getHeroes');
                dispatch('getBattles');
                dispatch('autoLogin');
            }
        },
        getHeroes({ commit }) {
            return new Promise(async (resolve, reject) => {
                try {
                    const response = await ApiService.loadHeroes();
                    commit('setHeroes', response.data.data);
                    resolve();
                } catch (e) {
                    // TODO: process error
                    reject(e);
                }
            });
        },
        saveHero({ state, dispatch }) {
            return new Promise(async (resolve, reject) => {
                try {
                    await ApiService.saveHero(state.heroData);
                    dispatch('getHeroes');
                    resolve();
                } catch (e) {
                    // TODO: process error
                    console.error(e);
                    reject(e);
                }
            });
        },
        deleteHero({ state, dispatch }) {
            return new Promise(async (resolve, reject) => {
                try {
                    await ApiService.deleteHero(state.selectedHero.id);
                    dispatch('getHeroes');
                    resolve();
                } catch (e) {
                    // TODO: process error
                    console.error(e);
                    reject(e);
                }
            });
        },
        getBattles({ commit }) {
            return new Promise(async (resolve, reject) => {
                try {
                    const response = await ApiService.loadBattles();
                    commit('setBattles', response.data.data);
                    resolve();
                } catch (e) {
                    // TODO: process error
                    console.error(e);
                    reject(e);
                }
            });
        },
        saveBattle({ state, dispatch }) {
            return new Promise(async (resolve, reject) => {
                try {
                    await ApiService.saveBattle(state.battleData);
                    // TODO: dispach battles
                    dispatch('getBattles');
                    resolve();
                } catch (e) {
                    // TODO: process error
                    console.error(e);
                    reject(e);
                }
            });
        },
        removeBattle({ state, dispatch }) {
            return new Promise(async (resolve, reject) => {
                try {
                    await ApiService.removeBattle(state.battleData.id);
                    // TODO: dispach battles
                    dispatch('getBattles');
                    resolve();
                } catch (e) {
                    // TODO: process error
                    console.error(e);
                }
            });
        },
        login({ state, commit }) {
            return new Promise(async (resolve, reject) => {
                let response;
                try {
                    response = await ApiService.login({ username: state.username, password: state.password });
                    if (response.data.status === 'error') {
                        commit('setLoginError', response.data);
                    }
                    commit('setSessionId', response.data.session_id);
                    resolve();
                } catch (e) {
                    // TODO: process error
                    reject(e);
                }
            });
        },
        autoLogin({ commit }) {
            return new Promise(async (resolve, reject) => {
                try {
                    const response = await ApiService.autoLogin();
                    commit('setSessionId', response.data.session_id);
                    resolve();
                } catch (e) {
                    // TODO: process error
                    reject(e);
                }
            });
        }
    },
    modules: {

    }
})
